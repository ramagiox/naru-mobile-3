import React, { Component } from "react";
import Intro from "./intro.js";
import Peta from "./peta.js";
import Home from "./home.js"
import PromoMenu from "./PromoMenu.js";
import Promo from "./Promo.js";
import News from "./News.js";
import Kontak from "./Kontak.js";
import Zakat from "./zakatInput.js";
import NewsDetail from "./newsDetail.js";
import Poi from "./poi.js";
import PromoDetail from "./promoDetail.js";
import Donasi from "./donasi.js";

import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
    Intro: {
        screen: Intro,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Donasi: {
        screen: Donasi,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    PromoDetail: {
        screen: PromoDetail,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Poi: {
        screen: Poi,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    NewsDetail: {
        screen: NewsDetail,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Zakat: {
        screen: Zakat,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    News: {
        screen: News,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Kontak: {
        screen: Kontak,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Peta: {
        screen: Peta,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    PromoMenu: {
        screen: PromoMenu,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Home: {
        screen: Home,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Promo: {
        screen: Promo,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    News: {
        screen: News,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
}));