import React from "react";
import { Image, AsyncStorage } from "react-native";
import Expo from "expo";
import getTheme from "./../../native-base-theme/components/";
import customColor from "./../../native-base-theme/variables/customColor";

import {
  StyleProvider,
  Header,
  Container,
  Content,
  Tab,
  Tabs,
  ScrollableTab,
  List,
  ListItem,
  Left,
  Body,
  Text
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"

export default class News extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      property: this.props.navigation,
      cat01: [],
      cat02: [],
      cat03: [],
      cat04: [],
      cat05: [],
      isReady: false,
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_event";
    fetchData(baseUrl + "/sale&event").then(data => {
      this.setState({ cat01: data });
      this.setState({ isReady: true });
    });
    fetchData(baseUrl + "/kuliner").then(data => { this.setState({ cat02: data }) });
    fetchData(baseUrl + "/wisata").then(data => { this.setState({ cat03: data }) });
    fetchData(baseUrl + "/mudik").then(data => { this.setState({ cat04: data }) });
    fetchData(baseUrl + "/bencana").then(data => { this.setState({ cat05: data }) });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <AppHeader title="Berita" />
        <StyleProvider style={getTheme(customColor)}>
          <Tabs renderTabBar={()=> <ScrollableTab />}>
            <Tab heading="Sale & Event">
              <ListContent news={{data : this.state.cat01, property: this.state.property }} />
            </Tab>
            <Tab heading="Kuliner">
              <ListContent news={{data : this.state.cat02, property: this.state.property }} />
            </Tab>
            <Tab heading="Wisata">
              <ListContent news={{data : this.state.cat03, property: this.state.property }} />
            </Tab>
            <Tab heading="Info Mudik">
              <ListContent news={{data : this.state.cat04, property: this.state.property }} />
            </Tab>
            <Tab heading="Bencana">
              <ListContent news={{data : this.state.cat05, property: this.state.property }} />
            </Tab>
          </Tabs>
        </StyleProvider>
        <AppFooter navigation={this.props.navigation} />
      </Container>
    );
  }
};

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("news", (error, result) => {
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then(response => response.json())
      .then((data) => {
        resolve(data);
      })
      .catch((e) => {
        reject(e);
      })
    })
  })
}
function ListContent(props) {
  const news = props.news.data;
  if (typeof news == 'undefined') {
    return (
      <Content>
        <Text>Tidak ada berita</Text>
      </Content>
    )
  }
  return (
    <Content>
      {
        news.length == 0 ?
        (
          <Text>Tidak ada berita</Text>
        ) : (
          <List>
            {news.map((item, idx) => {
              return (
                <ListItem button onPress={() => props.news.property.navigate("NewsDetail", { idBerita : item._id })}>
                  <Image source={{uri: item.newsImage}} style={{width: 120, height: 90, borderRadius: 5}} />
                  <Body>
                    <Text style={{fontSize: 14}}>{item.newsTitle}</Text>
                    <Text note style={{fontSize: 10, marginBottom: 5}}>{parseDate(item.newsStartDate)}</Text>
                    <Text note style={{fontSize: 12}}>{sliceNewsDescription(item.newsDescription)}</Text>
                  </Body>
                </ListItem>
              )
            })}
          </List>
        )
      }
    </Content>
  )
}
function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval+= "Senin"; break;
    case 1: retval+= "Selasa"; break;
    case 2: retval+= "Rabu"; break;
    case 3: retval+= "Kamis"; break;
    case 4: retval+= "Jumat"; break;
    case 5: retval+= "Sabtu"; break;
    case 6: retval+= "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval+= " Jan"; break;
    case 1: retval+= " Feb"; break;
    case 2: retval+= " Mar"; break;
    case 3: retval+= " Apr"; break;
    case 4: retval+= " Mei"; break;
    case 5: retval+= " Jun"; break;
    case 6: retval+= " Jul"; break;
    case 7: retval+= " Agu"; break;
    case 8: retval+= " Sep"; break;
    case 9: retval+= " Okt"; break;
    case 10: retval+= " Nov"; break;
    case 11: retval+= " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}
function sliceNewsDescription(text) {
  return text.length > 30 ? text.substring(0, 70) + "..." : text;
}