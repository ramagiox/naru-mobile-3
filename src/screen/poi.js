import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Slider from "react-native-slider";
import { Thumbnail, Text, Body, Left, Right, List, ListItem, Content, Container, Header, Picker } from 'native-base';
import { AppHeader, AppFooter } from '../app-nav/index';

const Item = Picker.Item;

export default class Poi extends Component {
  constructor(props) {
    super(props)
    this.state = {
      radius: 100,
      kategori: "SPBU"
    }
  }

  getVal(val) {
    console.log(val);
  }

  onValueChange2(value) {
    this.setState({
      kategori: value
    });
  }

  render() {
    return (
      <Container style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
        <AppHeader title="Point of Interest" />
        <Content>
          <Picker
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
            mode="dropdown"
            placeholder="Pilih Point of Interest"
            selectedValue={this.state.kategori}
            onValueChange={this.onValueChange2.bind(this)}
          >
            <Item label="SPBU" value="key0" />
            <Item label="Pos Polisi" value="key1" />
            <Item label="Masjid" value="key2" />
            <Item label="Restoran" value="key3" />
            <Item label="GRAPARI" value="key4" />
          </Picker>
        
          <Slider
            style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
            step={1}
            minimumValue={0}
            maximumValue={200}
            value={this.state.radius}
            onValueChange={val => this.setState({ radius: val })}
            onSlidingComplete={val => this.getVal(val)}
            
          />
          <Text>Radius: {this.state.radius ? <Text> { this.state.radius } </Text> : null}</Text>
          </Content>
        <AppFooter navigation={this.props.navigation} />
      </Container>
    );
  }

}
const styles = StyleSheet.create({
  imageBerita: {
    borderRadius: 3,
    marginLeft: "3%",
    marginRight: "3%",
    marginBottom: "3%",
    width: 30,
    height: 30,
  },
  wordBerita: {
    alignItems: 'flex-start',
  },
  wordRange: {
    fontFamily: 'Ubuntu_Regular',
    fontSize: 10,
    color: "#363636",
  },
  wordTitle: {
    paddingLeft: "3%",
    paddingRight: "3%",
    fontFamily: 'Gotham_Medium',
    fontSize: 16,
  },
  wordAddress: {
    fontFamily: 'Ubuntu_Light',
    fontSize: 12,
  },

});