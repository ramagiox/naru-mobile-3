/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    AppRegistry,
} from 'react-native';
import Carousel from 'react-native-carousel-view';
import {Container, Content, Button, Footer, Right} from 'native-base';

export default class Intro extends Component {
    render() {
        return (
            <Container>
            <Content style={styles.footer}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <View style={styles.container}>
                    <Carousel
                        width={400}
                        height={550}
                        delay={2000}
                        indicatorAtBottom={true}
                        indicatorColor="red">
                        <View style={styles.contentContainer}>
                            <Image
                                source={require('../../img/asset/onboarding_1.png')}
                            />
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Informasi Terkini</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Aplikasi TemanBerbagi memberikan</Text>
                            <Text style={styles.baseText}>Informasi terkini dan terpercaya</Text>
                            <Text style={styles.baseText}></Text>
               
                        </View>
                        <View style={styles.contentContainer}>
                            <Image
                                source={require('../../img/asset/onboarding_2.png')}
                            />
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Sekitar Kamu</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Aplikasi TemanBerbagi membantu kamu</Text>
                            <Text style={styles.baseText}>menemukan tempat penting sekitar kamu</Text>
                            <Text style={styles.baseText}>dan ikuti arah terceppat menuju kesana</Text>
                        </View>
                        <View style={styles.contentContainer}>
                            <Image
                                source={require('../../img/asset/onboarding_3.png')}
                            />
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.titleText}>Donasi</Text>
                            <Text style={styles.titleText}> </Text>
                            <Text style={styles.baseText}>Aplikasi TemanBerbagi memberikan kesempatan</Text>
                            <Text style={styles.baseText}>kepada kamu untuk menebar kebaikan</Text>
                            <Text style={styles.baseText}>bagi kemanusiaan di indonesia</Text>
                        </View>
                    </Carousel>
                </View>
            </View>
            </Content>
            <Footer style={styles.footer}>
            <Right>
            <Button style={{marginRight:"5%"}} transparent
                            onPress={() => this.props.navigation.navigate("Home")}
                        >
                            <Text uppercase={false} style={styles.baseText}>Lanjut</Text>
                        </Button>
                        </Right>
                        </Footer>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Ubuntu_Light',

    },
    titleText: {
        fontFamily: 'Ubuntu_Regular',
        fontSize: 30,
        fontWeight: 'bold',
    },
    container: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        backgroundColor: "#fff"
    },
});


AppRegistry.registerComponent('Intro', () => Intro);