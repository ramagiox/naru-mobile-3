import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem } from 'native-base';

export default class Kuliner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resto: [],
      isReady: false,
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/foods";
    fetchData(baseUrl).then(data => {
      this.setState({ resto: data, isReady: true });
    });
  }
  render() {
    const resto = this.state.resto.data;
    // console.log(resto);
    if ( typeof resto == 'undefined' ) {
      return <Text>Data Tidak ada</Text>;
    } else {
      if (!this.state.isReady) {
        return <Expo.AppLoading />;
      } else {
        return(
          <List>
            {
              resto.slice(0,5).map((item, idx) => {
                if ( item.opening_hours ) {
                  if ( item.opening_hours.open_now == true ) {
                    opening = "Buka";
                  } else {
                    opening = "Tutup";
                  }
                } else {
                  opening = "-";
                }
                return(
                  <View style={{flex: 1, flexDirection: 'column', borderBottomWidth: 1, borderColor:"#979797"}}>
                    <Text style={styles.wordTitle}>{item.name}</Text>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Thumbnail square source={require('../../img/asset/home/lokasi.png')} style={styles.imageBerita} />
                      <Body style={styles.wordBerita}>
                        <Text style={styles.wordAddress}>{sliceAddress(item.location)}</Text>
                        <Text style={styles.wordRange}>{opening}</Text>
                      </Body>
                    </View>
                  </View>
                )
              })
            }
          </List>
        );
      }
    }
  }

}

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("news", (error, result) => {
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
        },
        body: JSON.stringify({ gps: "-6.2088, 106.8456", radius: "500", types : "restaurant" })
      })
      .then(response => response.json())
      .then((data) => {
        resolve(data);
      })
      .catch((e) => {
        reject(e);
      })
    })
  })
}

function sliceAddress(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

const styles = StyleSheet.create({
    imageBerita: {
      borderRadius:3,
      marginLeft:"3%",
      marginRight:"3%",
      marginBottom:"3%",
      width: 30,
      height: 30,
    },
    wordBerita: {
      alignItems: 'flex-start',
    },
    wordRange: {
      fontFamily:'Ubuntu_Regular',
      fontSize: 10,
      color:"#363636",
    },
    wordTitle: {
      paddingLeft:"3%",
      paddingRight:"3%",
      fontFamily:'Gotham_Medium',
      fontSize: 16,
    },
    wordAddress: {
      fontFamily:'Ubuntu_Light',
      fontSize: 12,
    },
    
  });